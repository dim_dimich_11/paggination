import { Component, AfterViewInit, OnInit } from '@angular/core';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { PiePart } from './components/pie-chart/models/piePart';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.firstPart = {
      Color: '#26DFEC',
      Value: 30
    };
    this.secondPart = {
      Color: '#FAB010',
      Value: 20
    };

    this.thirdPart = {
      Color: '#F739AC',
      Value: 15
    };

    this.fourthPart = {
      Color: 'green',
      Value: 34
    };

    this.fivePart = {
      Color: 'red',
      Value: 1
    };
    this.chartPartsArr = [this.firstPart, this.secondPart, this.thirdPart, this.fourthPart, this.fivePart];
  }
  public chartPartsArr: Array<PiePart>;
  public firstPart: PiePart;
  public secondPart: PiePart;
  public thirdPart: PiePart;
  public fourthPart: PiePart;
  public fivePart: PiePart;
  
  title = 'paggination-app';
}
