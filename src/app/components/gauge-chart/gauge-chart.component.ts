import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'gauge-chart',
  templateUrl: './gauge-chart.component.html',
  styleUrls: ['./gauge-chart.component.scss']
})
export class GaugeChartComponent implements OnInit {

  @Input() selectedColor: string;
  @Input() unselectedColor: string;
  @Input() minValue: number;
  @Input() maxValue: number;
  @Input() selectedValue: number;
  @Input() mainRadius: number;
  public centerCoordinateX: number;
  public centerCoordinateY: number;
  public selectedPathX: number;
  public selectedPathY: number;
  public mainPath: string;
  public selectedPath: string;
  public smallRadius: number;
  public arrowStartX: number;
  public arrowStartY: number;
  public arrowEndX: number;
  public arrowEndY: number;

  constructor() { }

  ngOnInit() {
    this.centerCoordinateX = 150;
    this.centerCoordinateY = 150;
    this.selectedValue = +this.selectedValue < +this.minValue ? +this.minValue : +this.selectedValue;
    this.selectedValue = +this.selectedValue > +this.maxValue ? +this.maxValue : +this.selectedValue;
    this.smallRadius = 40;
    this.drawMainPath();
    this.drawSelectedPath();
    this.drawArrow();
  }

  public calculateDegrees() {
    const result = Math.PI * (1 - (this.selectedValue / this.maxValue));

    return result;
  }

  public calculateCoordinateX(radius: number) {
    const degrees = this.calculateDegrees();
    const result = this.centerCoordinateX + radius * Math.cos(degrees)

    return result;
  }

  public calculateCoordinateY(radius: number) {
    const degrees = this.calculateDegrees();
    const result = this.centerCoordinateY - radius * Math.sin(degrees);

    return result;
  }

  public drawMainPath() {
    const endX = (this.mainRadius * 2) + 50;
    this.mainPath = 'M50 150, A' + this.mainRadius + ' '
      + this.mainRadius + ', 0, 0 1, ' + endX + ' 150';
  }

  public drawSelectedPath() {
    this.selectedPathX = this.calculateCoordinateX(this.mainRadius);
    this.selectedPathY = this.calculateCoordinateY(this.mainRadius);
    this.selectedPath = 'M50 150, A' + this.mainRadius + ' ' + this.mainRadius + ', 0, 0 1, '
      + this.selectedPathX + ' ' + this.selectedPathY;
  }

  public drawArrow() {
    this.arrowStartX = this.calculateCoordinateX(this.smallRadius);
    this.arrowStartY = this.calculateCoordinateY(this.smallRadius);
    this.arrowEndX = this.calculateCoordinateX(75);
    this.arrowEndY = this.calculateCoordinateY(75);
  }
}
