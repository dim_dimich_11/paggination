import { Component, OnInit, Input } from '@angular/core';
import { PiePart } from './models/piePart';
import { PiePartStrokeConfiguration } from './models/piePartStrokeConfiguration';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {

  @Input() chartParts: Array<PiePart>;
  public partsConfigs: Array<PiePartStrokeConfiguration> = [];
  public centerX = 32;
  public centerY = 32;
  public length = Math.PI * 32;
  public textRadius = 20;

  constructor() { }

  ngOnInit() {
    this.initialize();
  }

  public initialize() {
    const total = this.chartParts.map(p => p.Value).reduce((a, b) => a + b, 0);

    this.chartParts.forEach((element, index) => {
      const percentage = element.Value / total;
      const absCenterDegree = this.getAbsoluteCenterDegree(element, total);
      const centerDegree = this.getCenterDegree(element, total);

      const partConfig = {
        Color: element.Color,
        Value: element.Value,
        DashArray: this.length.toString(),
        Dashoffset: (this.length * (1 - percentage)).toString(),
        TextX: this.calculateTextX(absCenterDegree),
        TextY: this.calculateTextY(absCenterDegree),
        Transform: `rotate(${this.getPreviousDegree(index, total) }, ${this.centerX}, ${this.centerY})`
      } as PiePartStrokeConfiguration;

      partConfig.OffsetX = this.getXray(centerDegree, 1.5) - this.centerX;
      partConfig.OffsetY = this.getYray(centerDegree, 1.5) - this.centerY;

      this.partsConfigs.push(partConfig);
    });
  }

  public convertDegreesToRadian(degrees: number) {
    const result = degrees * Math.PI / 180;
    return result;
  }

  private getXray(degrees: number, radius: number) {
    return this.centerX - radius * Math.cos(this.convertDegreesToRadian(degrees));
  }

  private getYray(degrees: number, radius: number) {
    return this.centerY - radius * Math.sin(this.convertDegreesToRadian(degrees));
  }

  public calculateTextX(degrees: number) {
    return this.getXray(degrees, this.textRadius);
  }

  public calculateTextY(degrees: number) {
    return this.getYray(degrees, this.textRadius);
  }

  public getPreviousDegree(index: number, total: number) {
    let val = 0;
    for (let previousIndex = 0; previousIndex < index; previousIndex++) {
      val += this.chartParts[previousIndex].Value;
    }

    return val / total * 360;
  }

  public getCenterDegree(element: PiePart, total: number) {
    return element.Value / total * 180;
  }

  public getAbsoluteCenterDegree(element: PiePart, total: number) {
    const idx = this.chartParts.indexOf(element);
    if (idx !== -1) {
      return 180 + this.getCenterDegree(element, total) + this.getPreviousDegree(idx, total);
    }

    throw new Error('Uknown pie part');
  }
}
