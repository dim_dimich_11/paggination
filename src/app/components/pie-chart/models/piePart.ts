export interface PiePart {
    Color: string;
    Value: number;
}
