export interface PiePartStrokeConfiguration {
    Dashoffset: string;
    DashArray: string;
    Value: number;
    Color: string;
    TextX: number;
    TextY: number;
    OffsetX: number;
    OffsetY: number;
    Transform: string;
}
